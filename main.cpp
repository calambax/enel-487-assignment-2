/*
 Project: Assignment 2 - Simple Complex Number Calculator
 Version: Interactive
 Author: Xiam Calamba
 Course: ENEL 487
 Date: October 23, 2015
 Description: The program asks for user's desired operation from a given selection.
 The user must then input desired floating point values to get the result. If the
 user tries to input an invalid operation, the program will prompt user to try again
 However,due to limitation of design, there is no suffecient feature to prevent user
 from inputting alphabetical values instead of floating points...

 */



#include<iostream>
#include<cstdio>
#include<cmath>
#include<ctype.h>
#include<stdlib.h>
#include<string>

using namespace std;

#define PI 3.14159265
#define MAX_CHAR 10
#define MAX_RI 10
#define MAX_NUMIN 256

struct ComplexNum
{
    double real_num;
    double img_num;
};

ComplexNum mul_func(ComplexNum a, ComplexNum b);
ComplexNum div_func(ComplexNum a, ComplexNum b);
ComplexNum exp_func(ComplexNum a);
ComplexNum inv_func(ComplexNum a);
bool checkInput(ComplexNum a, ComplexNum b);
char checkImgSign(double img_num);
void printAns(ComplexNum ans);
void getNum(char arr[], ComplexNum a, ComplexNum b);



int main()
{
    ComplexNum ans, a, b;
    bool done = 0;
    //char a_real_numchar[MAX_RI], a_img_numchar[MAX_RI], b_real_numchar[MAX_RI], b_img_numchar[MAX_RI];
    char user_in[MAX_CHAR];
    user_in[7] = '.';user_in[8] = '.';user_in[9] = '.';

      cout << "Complex Calculator" << endl << endl;
      cout << "Please select from the following selection: " << endl;
      cout << "A - Addition of two complex numbers" << endl;
      cout << "S - Subtraction of two complex numbers" << endl;
      cout << "M - Multiplication of two complex numbers" << endl;
      cout << "D - Division of two complex numbers" << endl;
      cout << "abs - Modulus/absolute value of a complex number" << endl;
      cout << "arg - Argument of a complex number in radians" << endl;
      cout << "argDeg - Argument of a complex number in degrees" << endl;
      cout << "exp - Exponential of a complex number in degrees" << endl;
      cout << "inv - Inverse of a complex number in degrees" << endl;
      cout << "Q - to exit program" << endl;
      cout << "WARNING: Entering an alphabetical value will result to program failure" << endl;


    while (!done)
    {
        cout << "Enter exp:";
        cin >> user_in;
        //if ((user_in[0] == 'q') || (user_in[0] == 'Q'))
        if ((((user_in[0] == 'q') || (user_in[0] == 'Q')) && (user_in[1] == 0)) || (((user_in[0] == 'q') || (user_in[0] == 'Q')) && ((user_in[1] == 'u') || (user_in[1] == 'U'))
            && ((user_in[2] == 'i') || (user_in[2] == 'I')) && ((user_in[3] == 't') || (user_in[3] == 'T'))))
        {

                done = 1;
                cout << "Program will now exit." << endl;

        }
        else
        {
            if (isalpha(user_in[0]))
            {
                //a.real_num = 0; a.img_num = 0;b.real_num = 0; b.img_num = 0;
                if ((((user_in[0] == 'a') || (user_in[0] == 'A')) && ((user_in[1] == 'd') || (user_in[1] == 'D'))
                        && ((user_in[2] == 'd') || (user_in[2] == 'D'))) || (((user_in[0] == 'a') || (user_in[0] == 'A'))
                        && ((user_in[1] == 0) || (user_in[1] == 0))))
                {
                    cout << "You chose Addition." << endl;
//                    while (!input_pass)
//                    {

                        cout << "Enter parameters: ";
                        cin >> a.real_num >> a.img_num >> b.real_num >> b.img_num;
//                        if(checkInput(a.real_num,a.img_num,b.real_num,b.img_num) == true)
//                        {
//                            input_pass = true;
//                        }
//                        else
//                        {
//                            input_pass = false;
//                        }
//                    }comp_num

                    ans.real_num = a.real_num + b.real_num;
                    ans.img_num = a.img_num + b.img_num;
                    //cout << "Parameters are: " << a.real_num << " i"
                    //    << a.img_num << " " << b.real_num << " i"
                    //    << b.img_num << endl;
                    cout << "The sum is: ";
                    printAns(ans);

                }
                else if ((((user_in[0] == 's') || (user_in[0] == 'S')) && ((user_in[1] == 'u') || (user_in[1] == 'U'))
                          && ((user_in[2] == 'b') || (user_in[2] == 'B'))) || (((user_in[0] == 's') || (user_in[0] == 'S'))
                          && ((user_in[1] == 0) || (user_in[1] == 0))))
                {
                    cout << "You chose Subtraction" << endl;
                    cout << "Enter parameters: " ;
                    //getNum(user_in,a,b);
                    cin >> a.real_num >> a.img_num >> b.real_num >> b.img_num;
                    ans.real_num = a.real_num - b.real_num;
                    ans.img_num = a.img_num - b.img_num;
                    cout << "The difference is: ";
                    printAns(ans);

                }
                else if ((((user_in[0] == 'm') || (user_in[0] == 'M')) && ((user_in[1] == 'u') || (user_in[1] == 'U'))
                          && ((user_in[2] == 'l') || (user_in[2] == 'L'))) || (((user_in[0] == 'm') || (user_in[0] == 'M'))
                          && ((user_in[1] == 0) || (user_in[1] == 0))))
                {
                    cin >> a.real_num >> a.img_num >> b.real_num >> b.img_num;
                    cout << "You chose Multiplication" << endl;
                    cout << "Parameters are: " << a.real_num << " i"
                        << a.img_num << " " << b.real_num << " i"
                        << b.img_num << endl;
                    ans = mul_func(a,b);
                    cout << "The product is: ";
                    printAns(ans);

                }
                else if ((((user_in[0] == 'd') || (user_in[0] == 'D')) && ((user_in[1] == 'i') || (user_in[1] == 'I'))
                          && ((user_in[2] == 'v') || (user_in[2] == 'V'))) || (((user_in[0] == 'd') || (user_in[0] == 'D'))
                          && ((user_in[1] == 0) || (user_in[1] == 0))))
                {
                    cin >> a.real_num >> a.img_num >> b.real_num >> b.img_num;
                    cout << "You chose Division" << endl;
                    cout << "Parameters are: " << a.real_num << " i"
                        << a.img_num << " " << b.real_num << " i"
                        << b.img_num << endl;
                    ans = div_func(a,b);
                    cout << "The quotient is: ";
                    printAns(ans);

                }
                else if((((user_in[0] == 'a') || (user_in[0] == 'A')) && ((user_in[1] == 'b') || (user_in[1] == 'B'))
                       && ((user_in[2] == 's') || (user_in[2] == 'S'))) && ((user_in[3] == 0) || (user_in[3] == 0)))
                {
                    cin >> a.real_num >> a.img_num;
                    double abs;
                    cout << "You have chosen modulus." << endl;
                    abs = sqrt((a.real_num*a.real_num)+(a.img_num*a.img_num));
                    cout << "The magnitude is: " << abs << endl;

                }
                else if( ((user_in[0] == 'a') || (user_in[0] == 'A')) && ((user_in[1] == 'r') || (user_in[1] == 'R'))
                         && ((user_in[2] == 'g') || (user_in[2] == 'G'))&& ((user_in[3] == 0) || (user_in[3] == 0)))
                {
                    cin >> a.real_num >> a.img_num;
                    double arg;
                    cout << "You have chosen argument in radians." << endl;
                    arg = atan (a.img_num/a.real_num); //* 180 / PI;
                    cout << "The argument in radians is: " << arg << endl;
                }
                else if ( ((user_in[0] == 'a') || (user_in[0] == 'A')) && ((user_in[1] == 'r') || (user_in[1] == 'R'))
                          && ((user_in[2] == 'g') || (user_in[2] == 'G')) && ((user_in[3] == 'd') || (user_in[3] == 'D'))
                          && ((user_in[4] == 'e') || (user_in[4] == 'E')) && ((user_in[5] == 'g') || (user_in[5] == 'G'))
                          && ((user_in[6] == 0 ) || (user_in[6] == 0 )))
                {
                    cin >> a.real_num >> a.img_num;
                    double argDeg;
                    cout << "You have chosen argument in degrees." << endl;
                    argDeg = atan (a.img_num/a.real_num) * 180 / PI;
                    cout << "The argument in degrees is: " << argDeg << endl;
                }
                else if (((user_in[0] == 'e') || (user_in[0] == 'E')) && ((user_in[1] == 'x') || (user_in[1] == 'X'))
                        && ((user_in[2] == 'p') || (user_in[2] == 'P'))&& ((user_in[3] == 0) || (user_in[3] == 0)))
                {
                    cin >> a.real_num >> a.img_num;
                    ComplexNum expon;
                    cout << "You have chosen exponential function." << endl;
                    expon = exp_func(a);
                    cout << "The exponential is: ";
                    printAns(expon);
                }
                else if (((user_in[0] == 'i') || (user_in[0] == 'I')) && ((user_in[1] == 'n') || (user_in[1] == 'N'))
                        && ((user_in[2] == 'v') || (user_in[2] == 'V'))&& ((user_in[3] == 0) || (user_in[3] == 0)))
                {
                    cin >> a.real_num >> a.img_num;
                    ComplexNum inv;
                    cout << "You have chosen inverse exponential function." << endl;

                    cout << "The reciprocal is: ";
                    printAns(inv);
                }
                else
                {
                    cerr << "The input '" << user_in << "' does not correspond to a valid command input." << endl;
                    cerr << "Please try again." << endl;
                }
            }
        }
    }
    return 0;
}

//void getNum(char arr[], ComplexNum a, ComplexNum b)
//{
//    bool done = 0;
//    int strlen;
//    string user_in;
//    while(!done)
//    {
//    cin >> user_in;
//    strlen = user_in.length();
//    for (int i = 0; i < strlen; i++)
//    {
//        cout << i;
//    if (isalpha(user_in[i]))
//    {
//        cout << user_in[i] << " is not a number. Please try again." << endl;

//    }
//    else
//    {
//        arr = (const char*)     user_in.c_str();
//        fgets(arr,MAX_CHAR,stdin);
//        a.real_num = atof(arr);
//        done = 1;
//    }
//    }
//    cout << " a_real = " << a.real_num << endl;
//    }

//}

void printAns(ComplexNum ans)
{
    char img_sign;
    img_sign = checkImgSign(ans.img_num);
    double abs_ans_img_num;
    abs_ans_img_num = abs(ans.img_num);
    cout << ans.real_num << " " << img_sign << " j " << abs_ans_img_num << endl;
}

char checkImgSign(double img_num)
{
    char img_sign;
    if (img_num < 0)
    {
        img_sign = '-';
    }
    else
    {
        img_sign = '+';
    }
    return img_sign;
}

ComplexNum exp_func(ComplexNum a)
{
    ComplexNum expon;
    expon.real_num = exp(a.real_num) * cos(a.img_num);
    expon.img_num = exp(a.real_num) * sin(a.img_num);
    return expon;
}

ComplexNum inv_func(ComplexNum a)
{
    ComplexNum inv;
    inv.real_num = a.real_num / ((a.real_num*a.real_num)+(a.img_num*a.img_num));
    inv.img_num = -a.img_num / ((a.real_num*a.real_num)+(a.img_num*a.img_num));
    return inv;
}

ComplexNum mul_func(ComplexNum a, ComplexNum b)
{
    ComplexNum ans;
    double a_polar, b_polar, polar_product, a_angle, b_angle, polar_angle;
    a_polar = sqrt((a.real_num*a.real_num)+(a.img_num*a.img_num));
    b_polar = sqrt((b.real_num*b.real_num)+(b.img_num*b.img_num));
    polar_product = a_polar*b_polar;
    a_angle = (atan(a.img_num/a.real_num) * 180) / PI;
    b_angle = (atan(b.img_num/b.real_num) * 180) / PI;
    polar_angle = a_angle+b_angle;
    ans.real_num = (cos(polar_angle * PI / 180)) * polar_product;
    ans.img_num = (sin(polar_angle * PI /180)) * polar_product;
    return ans;
}


ComplexNum div_func(ComplexNum a, ComplexNum b)
{
    ComplexNum ans;
    double a_polar, b_polar, polar_product, a_angle, b_angle, polar_angle;
    a_polar = sqrt((a.real_num*a.real_num)+(a.img_num*a.img_num));
    b_polar = sqrt((b.real_num*b.real_num)+(b.img_num*b.img_num));
    polar_product = a_polar/b_polar;
    a_angle = atan(a.img_num/a.real_num) * 180 / PI;
    b_angle = atan(b.img_num/b.real_num) * 180 / PI;
    polar_angle = a_angle-b_angle;
    ans.real_num = (cos(polar_angle * PI / 180)) * polar_product;
    ans.img_num = (sin(polar_angle * PI /180)) * polar_product;
    return ans;
}

